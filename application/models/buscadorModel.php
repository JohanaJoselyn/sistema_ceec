<?php if( ! defined('BASEPATH'))  exit('No direct script access allowed');


class buscadorModel extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
	}


function vertodo($bus){


//BUSCADOR
	$this->db->like('id_curso', $bus);
	$this->db->or_like('nombre_curso', $bus);
	$this->db->or_like('archivos_link', $bus);
	$this->db->or_like('horas_cursos', $bus);
	$this->db->or_like('cantidad_alumnos', $bus);
	$this->db->or_like('nota_curso', $bus);
	$this->db->or_like('nombre_programa', $bus);
	$this->db->or_like('estado', $bus);

//CONSULTA	
	$this->db->select('c.id_curso,c.nombre_curso,c.archivos_link,c.horas_cursos,c.cantidad_alumnos,c.nota_curso,p.nombre_programa,p.estado,c.id_programa');
	$this->db->from('programas p');
	$this->db->join('curso c', 'c.id_programa = p.id_programa');

	$query = $this->db->get();

	//$query2 = $this->db->get('programas');
	

	if ($query->num_rows() > 0  ) {
		return $query;
	}else{
		echo "<script>alert('No hay registros que coincidan con el criterio de búsqueda');</script>";

		return $query;
		
		}
}


function obtenerEstado($id){
	
	//$this->db->select('p.id_programa,p.estado');
	//$this->db->from('programas p');
	//$this->db->join('curso c', 'c.id_programa = p.id_programa');

	$this->db->where('id_programa',$id);
	$query=$this->db->get('programas');

	if ($query->num_rows() > 0) {
	 	return $query;
	 }else{
	 	return $query; 
	 }
}

function editarEstado($id, $data){
	$this->db->where('id_programa',$id);	
	$this->db->update('programas',$data);
}

}

?>