<?php if( ! defined('BASEPATH'))  exit('No direct script access allowed');


class visualizacionModel extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function obtenervalores($id){

	$this->db->select('nombre_curso,id_curso,comuna_curso,region_curso,tipo_curso,salida_curso,componentes_curso,cantidad_alumnos,p.nombre_programa');
	$this->db->where('id_curso',$id);
	
	$this->db->from('programas p');
	$this->db->join('curso c', 'c.id_programa = p.id_programa');
	$query = $this->db->get();

	if ($query->num_rows() > 0) {
	 	return $query;
	 }else{
	 	return $query; 
	 }

	}

//SELECCIONAR TODO DE LAS TABLAS CUANDO EN LA TABLA EQUIPO EL NRO_PROPUESTA SEA IGUAL AL NRO_PROPUESTA DEL CURSO.- 
//SELECCIONAR TODO DE LA TABLA PROGRAMAS CUANDO LA ID DE ENTIDAD DE PROGRAMAS SEA IGUAL A LA ID DE ENTIDAD DE ENTIDAD.-

function selecciones($id){
		$this->db->select('*');

		$this->db->from('curso c','programas p');
		$this->db->join('equipos_herramientas_otic eq', 'eq.nro_propuesta = c.nro_propuesta');
		$this->db->where('id_curso',$id);

		$this->db->from('programas p');
		$this->db->join('entidad e', 'p.id_entidad = e.id_entidad');


		$this->db->join('modulos_curso_otic mo', 'mo.nro_propuesta = c.nro_propuesta');

	


		$query = $this->db->get();

		if ($query->num_rows() > 0) {
		 	return $query;
		
		}else{
			return $query;
		}

	}

	function combobox(){
	$this->db->select('nombre');
	$this->db->from('entidad');
	$query = $this->db->get();

	if ($query->num_rows()>0) {
		return $query->result();

	}
}

//esto no sirve

function insertar($data){

	$query = $this->db->insert('curso',$data);
	if ($query>0) {
		return true;
	}
	else{
		return false;
	}
}

		function verentidad(){
		$this->db->select('*');
		$this->db->from('entidad e');
		$this->db->join('programas p', 'p.id_entidad = e.id_entidad');
		$query= $this->db->get();
		

		if ($query->num_rows() > 0) {
		 	return $query->result();
		
		}
	}


}
