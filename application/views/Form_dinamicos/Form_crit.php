<!DOCTYPE html>
<html>
<head>
	<title>Criterios Evaluación </title>

	<link rel="stylesheet" type="text/css" href="<?= base_url();?>/css/bootstrap.min.css">
	<link rel="stylesheet"  href="<?= base_url();?>/css/prueba.css" type="text/css" >

	<script src="<?php echo base_url()?>/css/external/jquery/jquery.js"></script>
	<script src="<?php echo base_url()?>/css/external/jquery/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url()?>http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="<?php echo base_url()?>http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	<script src="<?php echo base_url()?>https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

	<link rel="shortcut icon" type="image/png" href="<?= base_url();?>/css/images/favicon.png"/>

	<img  src="<?= base_url();?>/css/images/logo.png">

	<h2 align="center">Formulario</h2>

	<script>

		//FUNCION PARA LOS COMBOBOX ANIDADOS C:

		$(function(){
			$.ajaxSetup({
				type : 'POST',
				url : '<?php echo base_url("criterios/anidado")?>',
				cache : false
			});
			$("#id_entidad").change(function(){
				var value = $(this).val();
				if (value>0) {
					$.ajax({
						data:{nombre_programa:'programas',id_entidad:value},
						success: function(respon){
							$("#nombre_programa").html(respon);

						}
					});
				}
			});

		});
	
		//FUNCION PARA ABRIR VENTANAS DE OTOC Y FOSIS (FORMULARIOS ESTATICOS)
	    function OnChange(sel) {
	      if (sel.value=="a"){
	           divC=window.open("<?=base_url();?>/criterios/cargarotic").style.display="block";
	           divC.style.display = "";

	           divT = window.open("<?=base_url();?>/criterios/cargarfosis");
	           divT.style.display = "none";

	      }if (sel.value=="b"){

	           divC = window.open("<?=base_url();?>/criterios/cargarfosis");
	           divC.style.display="none";

	           divT = window.open("<?=base_url();?>/criterios/cargarfosis");
	           divT.style.display = "";
	      }
		}
	</script>
</head>

<body>
	<br>

	<?php $this->load->helper('form');
	echo form_open (base_url().'/criterios/haceralgo') ;?>

	<div style="text-align:center">
		
	<label >Entidad: </label>

	<select  id="id_entidad" name="id_entidad" onchange="OnChange(this)">
		<option  value="0" >Seleccione</option>
		<option  value="a" >OTIC</option>
		<option  value="b" >FOSIS</option>

		<?php

		foreach ($entidad as $e) {
			
			echo '<option value="'.$e->id_entidad.'">'.$e->nombre.'</option>';
		
		}
		?>
	</select>


	

	<select  id="nombre_programa" name="nombre_programa">
		<option   value="0"> Seleccione </option>
		
	</select>

	</div>

	


<br><br>
	<?php echo form_close(); ?>

	<form action="<?= base_url();?>/criterios/check" method="POST">
		
	<table id="tabla-archivos" class="table table-striped table-bordered dt-responsive nowrap table-hover table-condensed"   style="background: white!important">

		<tr>
			<th class=" bg-primary">
				Información general
			</th>
		</tr>

		<tr>
			
				<td style="width: 15%" align="justify">
				
			

			
			1.- Información general 
				
				<input class="check" type="checkbox" id="Informacion" name="Informacion" value="" checked="">
				
				<br><br>
			

			2.- Entidad 
				
				<input align="center" class="check" type="checkbox" id="entidad" name="entidad" value="">
				
				<br><br>

			3.- Curso 
				
				<input class="check" type="checkbox" id="Curso" name="Curso" value="">
				
				<br><br>

			4.- Otros datos 
			
				<input class="check" type="checkbox" id="Otros" name="Otros" value="">
				
				<br><br>

			5.- Identificación propuesta y proponente Fosis 
			
				<input class="check" type="checkbox" id="identificacion" name="identificacion" value="">
				
				<br><br>

			</td>
			
		</tr>
	</table>




	<br>
	<table  id="tabla-archivos" class="table table-striped table-bordered dt-responsive nowrap table-hover table-condensed"   style="background: white!important">
		<tr>
			
			<th class=" bg-primary">Metodología</th>
			<th class=" bg-primary">Instrumentos</th>
			<th class=" bg-primary">Recursos Humanos</th>
		</tr>

		<tr >

		
			<td style="width: 20%">
				<!-- PORCENTAJES OBLIGATORIOS-->
				<!-- A.- Metodología-->

				1.- Módulos del curso 
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check"  type="checkbox" id="Modulos" name="Modulos" value="">
				
				<br><br>

				2.- Aprendizaje 
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input  class="check" type="checkbox" id="Aprendizaje" name="Aprendizaje" value="">
				
				<br><br>

				3.-Contenido
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Contenido" name="Contenido" value="">
				
				<br><br>

				4.- Metodología OTIC
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Metodologia" name="Metodologia" value="" >
				
				<br><br>

				5.-Criterios de evaluación
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Criterios" name="Criterios" value="">
				
				<br><br>

				6.- Duración del curso 
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Duración" name="Duración" value="">
				
				<br><br>

				7.- Metodología FOSIS  
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="metodologia_fosis" name="metodologia_fosis" value="">
				
				<br><br>		

				7.1.-Enfoque génerico
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Enfoque" name="Enfoque" value="">
				
				<br><br>

				7.2.-Innovación y valor agregado
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Innovacion" name="Innovacion" value="" >
				
				<br><br>

				8.- Propuesta FOSIS
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="propuesta" name="propuesta" value="">
				
				<br><br>

				





			</td>

			<td style="width: 20%">
				<!-- B.-INSTRUMENTOS-->
				<!-- PORCENTAJES OBLIGATORIOS-->


				1.- Materiales e insumos OTIC
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Materiales" name="Materiales" value="">
				
				<br><br>

				2.- Equipos 
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Equipos" name="Equipos" value="">
				
				<br><br>

				3.- Utiles 
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Utiles" name="Utiles" value="">
				
				<br><br>

				4.- Infraestructura OTIC
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Infraestructura" name="Infraestructura" value="">
				
				<br><br>

				4.- Infraestructura y equipamiento FOSIS
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Infraestructuraf" name="Infraestructuraf" value="">
				
				<br><br>

				5.- Estructura del costo  
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Estructura" name="Estructura" value="">
				
				<br><br>

				6.- Presupuesto  
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Presupuesto" name="Presupuesto" value="">
				
				<br><br>

				7.- Difusión
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Difusion" name="Difusion" value="">
				
				<br><br>


				

				<!-- PORCENTAJES CON VALUE 0% OTIC-->

				

				<!-- PORCENTAJES CON VALUE 0% FOSIS-->

				


			</td>

			<td style="width: 30%">
				<!-- PRESUPUESTO-->
				<!-- PORCENTAJES OBLIGATORIOS-->

				

				1.- Formación profesional o técnica en tématica de género, discapacidad e interculturidad. 
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Formacion" name="Formacion" value="">
				
				<br><br><br><br>

				2.- Dedicación y organización del equipo profesional. 
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Dedicación" name="Dedicación">
				
				<br><br><br>

				3.- Experiencias del equipo en cargo y componentes (perfil y funciones) y conocimiento de la población objetivo. 
				<input placeholder="0%" type="text" style="width : 35px; heigth : 35px">
				<input class="check" type="checkbox" id="Experiencias" name="Experiencias" value="">
				
				<br><br>
				
			</td>
				

		</tr>
	</table>

<br><br>
	<button  class="crear" type="submit">Crear formulario</button>


</form>
<br><br>



</body>
	
	<script src="<?php echo base_url()?>/css/bootstrap.min.js"></script>
</html>