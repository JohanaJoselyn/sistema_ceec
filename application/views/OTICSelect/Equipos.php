<!DOCTYPE html>
<html>
<head>
	<title>Equipos y herramientas</title>
    <script type="text/javascript">

$(document).ready(function(){
        
       

        $("#add4").click(function(){
            // Obtenemos el numero de filas (td) que tiene la primera columna
            // (tr) del id "tabla"
            var tds=$("#tabla4 tr:first td").length;
            
            // Obtenemos el total de columnas (tr) del id "tabla"
            var trs=$("#tabla4 tr").length;
            var nuevaFila="<tr>";
            
            for(var i=3;i<tds;i++){

                // añadimos las columnas
            nuevaFila +="<td><input type='Text' size='80' name='NroModulo' ></td>"+
                "<td><input type='Number' name='CantidadM' onkeypress='return check(event)'> </td>"+
                "<td><input type='Number' name='Antiguedad' > </td>"+
                "<td><input type='text' size='25' name='CertificacionAso'></td>";
            }
            // Añadimos una columna con el numero total de filas.
            // Añadimos uno al total, ya que cuando cargamos los valores para la
            // columna, todavia no esta añadida
            //nuevaFila+="<td>"+(trs+1)+" filas";
            //nuevaFila+="</tr>";
            $("#tabla4").append(nuevaFila);
        });

         /**
         * Funcion para eliminar la ultima columna de la tabla.
         * Si unicamente queda una columna, esta no sera eliminada
         */
        $("#del4").click(function(){
            // Obtenemos el total de columnas (tr) del id "tabla"
            var trs=$("#tabla4 tr").length;
            if(trs>2)
            {
                // Eliminamos la ultima columna
                $("#tabla4 tr:last").remove();
            
            
            }
        });

    });



    //la funcion check permite que solo se ingresen numeros en las casillas donde se requiera 
    function check(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8 ) {
         return true;
        }

        // Patron de entrada, en este caso solo acepta numeros 
        patron = /[0-9,]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }
    
</script>
</head>
<body>
	<form>


<div class="accordion" id="accordionExample275">
  <div class="card z-depth-0 bordered">
    <div class="card-header" id="headingeq">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseeq"
          aria-expanded="true" aria-controls="collapseeq">
   <h2>B) Equipos y herramientas</h2>


        </button>
      </h5>
    </div>
    <div id="collapseeq" class="collapse" aria-labelledby="headingeq" data-parent="#accordionExample275">
      <div class="card-body">
    
  <table>

    <!--Las Id seran las que se referencian en las deferentes funciones Del Script -->
                <td><input name="button" id="add4"  type=button onclick="agregar()" value="Agregar Fila" ></td>
                <td><input name="button" id="del4" type=button onclick="borrarUltima()" value="Eliminar Fila" ></td>
    </table>

        <table id="tabla4" border="1 px">

            
            <tr>
                <td><strong>Descripción</strong></td>
                <td><strong>Cantidad</strong></td>
                <td><strong>Antiguedad</strong></td>
                <td><strong>Certificación normas asociadas</strong></td>
            </tr>
            <tr>
                <td><input value="<?=$data['descripcion']?>" type='Text' size="80" name='NroModulo' > </td>
                <td><input value="<?=$data['CantidadM']?>" type='Number' name='CantidadM' onkeypress='return check(event)'> </td>
                <td><input value="<?=$data['Antiguedad']?>" type='Number' name='Antiguedad' > </td>
                <td><input value="<?=$data['CertificacionAso']?>" type='text' size='25' name='CertificacionAso'></td>
            </tr>
            </table>

      </div>
    </div>
  </div>
  
</div>



    
   
       
        <br>
	</form>

</body>
<script src="<?php echo base_url()?>js/jquery.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>

</html>