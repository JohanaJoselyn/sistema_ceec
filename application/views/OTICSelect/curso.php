<!DOCTYPE html>
<html>
<head>



	<title>Identificación del Curso</title>
</head>
<body>

	<form>


<div class="accordion" id="accordionExample275">
  <div class="card z-depth-0 bordered">
    <div class="card-header" id="headingiii">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseiii"
          aria-expanded="true" aria-controls="collapseiii">
<h2> III.- Identificación del Curso</h2>

        </button>
      </h5>
    </div>
    <div id="collapseiii" class="collapse" aria-labelledby="headingiii" data-parent="#accordionExample275">
      <div class="card-body">
    


		<div class="tercero">
		<label class="arregloIII">- Nombre Curso </label><input type="Text" id="nombre_curso" name="nombre_curso" value="<?=$data['nombre_curso']?>"></> <br>

		<label class="arregloIII">- Código del curso </label><input type="Text" id="codigo_curso" name="codigo_curso" value="<?=$data['codigo_curso']?>"/><br>

		<label class="arregloIII">- Comuna de ejecución del curso </label><input type="Text" id="comuna_curso"name="comuna_curso" value="<?=$data['comuna_curso']?>"/> <br>

		<label class="arregloIII">- Región </label><input type="Text" id="region_curso" name="region_curso" value="<?=$data['region_curso']?>"/> <br>

		<label class="arregloIII">- Tipo De Curso </label><input type="Text" id="tipo_curso" name="tipo_curso" value="<?=$data['tipo_curso']?>"/> <br>

		<label class="arregloIII">- Tipo de Salida <img src="<?= base_url();?>/css/images/icon2.png" display="true" title=" Dependiente o independiente"></label><input type="Text" id="salida_curso" name="salida_curso" value="<?=$data['salida_curso']?>"/> <br>

		<label class="arregloIII">- Cupo </label> <input type="Text" id="cupos_curso" name="cupos_curso" value="<?=$data['cupos_curso']?>"/> <br>

		<label class="arregloIII">- Nombre Componentes Transversales del Curso <img src="<?= base_url();?>/css/images/icon2.png" display="true" title="Según corresponda"></label> <input type="Text" id="componentes_curso" name="componentes_curso" value="<?=$data['componentes_curso']?>"/> <br>

		<label>Programa</label><input type="Text" id="nombre_programa" name="nombre_programa" value="<?=$data['nombre_programa']?>" />

		<label>Entidad</label>

		<select  id="id_entidad" name="id_entidad" >
			<option>Seleccione</option>
			<?php

			foreach ($entidad as $e) {
				
				echo '<option value="'.$e->id_entidad.'">'.$e->nombre.'</option>';
			
			}
			?>
	</select>
		</div>	
		<br>

      </div>
    </div>
  </div>
  
</div>
		
	
		
	</form>
 

</body>

<script src="<?php echo base_url()?>js/jquery.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
</html>