<!DOCTYPE html>
<html>
<head>
	<title>vista de editar</title>
	<link rel="stylesheet"  href="<?= base_url();?>/css/prueba.css" type="text/css" >
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>/css/bootstrap.min.css">
	<link rel="shortcut icon" type="image/png" href="<?= base_url();?>/css/images/favicon.png"/>


</head>
<body class="a">
	<!-- esto es un comentario de prueba para git-->
	<img  src="<?= base_url();?>/css/images/logo.png">
	<br><br><br><br><br><br>
	<div id="login">
		<div class="login-triangle"></div>
		 <h2 class="login-header">Estados</h2>
 		 
	
	<form class="login-container" action="<?php echo base_url()?>buscador/editarEstado/<?=$id?>" method="POST">

		
		<p>Solo se admiten dos tipos de estados; Adjudicado o perdido </p>
		<p class="input">Estado: <input disabled type="text" name="estado" id="estado" placeholder="editar estado" value="<?=$estado?>" /><br></p>

		<select name="estado" id="estado" >
			<option value="seleccione" selected="selected" disabled="">Seleccione</option>
			<option value="Adjudicado" >Adjudicado</option>
			<option value="Perdido">Perdido</option>
		</select>

		<p></p>
	
			<input  class="btn btn-primary-float=right" id="cerrar" onclick="window.close();" type="submit" name="Cerrar" value="Cerrar"> 
			<input  class="btn btn-primary-float=left" id="editar"  type="submit" name="Editar" value="Editar"> 
				
			

	
		
		
		<br><br><br>
	</form>
	</div>
	
	
	<script src="<?php echo base_url()?>js/jquery.js"></script>
	<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
</body>
</html>