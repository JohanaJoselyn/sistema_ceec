<!DOCTYPE html>

<html>

<head>
	<title>Filtros de búsqueda</title>
	
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>/css/bootstrap.min.css">
	<link rel="stylesheet"  href="<?= base_url();?>/css/prueba.css" type="text/css" >
	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	<link rel="shortcut icon" type="image/png" href="<?= base_url();?>/css/images/favicon.png"/>

	

</head>
<body>
<h2 align="center">Busqueda de propuesta</h2>

    <img  src="<?= base_url();?>/css/images/logo.png">

	<form action="" method="post" align="center" >
		<br><br>
	<div class="search-box">
		
	
		
		<input  class="search-txt"  name="busqueda" type="text" placeholder="Buscar" results="0">

		<button class="search-btn" href="#">
			<i class="fas fa-search"></i>
		</button>
		

		<!--
		<button class="btn btn-primary" type="submit">
			
			<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
		</button> 
-->	</div>

	</form>


	<br><br>
	<table id="tabla-archivos" class="table table-striped table-bordered dt-responsive nowrap table-hover table-condensed"   style="background: white!important">
		<thead>
		<tr>
			<th class=" bg-primary">Código</th>
			<th class="bg-primary">Curso</th>
		
			<th  class="bg-primary">PDF</th>
			<th  class="bg-primary">Horas</th>
			<th  class="bg-primary">Cantidad alumnos</th>
			<th  class="bg-primary">Nota curso</th>
			<th  class="bg-primary">Programas</th>
			<th  class="bg-primary">Estado    </th>
			<th  class="bg-primary">Acciones Formulario</th>
		</tr>	
		</thead>
		<?php
			foreach ($curso->result() as $cursos ) {
				
				
				echo "<tr>";

				
					echo "<td align='center'>".$cursos->id_curso."</td>";
					echo "<td align='center'>".$cursos->nombre_curso."</td>";
					echo "<td align='center'>". $cursos->archivos_link."</td>";
					echo "<td align='center'>". $cursos->horas_cursos."</td>";
					echo "<td align='center'>". $cursos->cantidad_alumnos."</td>";
					echo "<td align='center'>". $cursos->nota_curso."</td>";

					echo "<td align='center'>".$cursos->nombre_programa."</td>";
					echo "<td align='center'>".$cursos->estado." |
					|<a href=".base_url()."buscador/editar/".$cursos->id_programa." target='_blank'>
		 				<i class='fas fa-edit'></i>
		 			</a
					</td>";

					

		 			echo "<td align='center'>
		 			<a href=".base_url()."buscador/verArchivo/".$cursos->id_curso." target='_blank'>
		 				<i class='fas fa-eye'></i>
		 			</a> ||
		 			<a href=".base_url()."visualizacion/visualizar/".$cursos->id_curso." target='_blank' >
		 				<i class='fas fa-book-open'></i>
		 			</td>";


					 ?>

					
					 <!--
					 <td>	 
					 <a href="<?=base_url()?>archivo/verArchivo/<?=$cursos->id_curso ?>" target="_blank">verdff<i class="fa fa-eye" aria-hidden="true"></i></a>
					</td>
					-->
<?php
      "</td>";  ?>

				 
			 		<br></br>
			<?php echo "</tr>";
				
 
			}
				$this->load->helper('url'); 
			
			 ?>
	</table>
	<script src="<?php echo base_url()?>js/jquery.js"></script>
	<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>


	
	
	
</html>