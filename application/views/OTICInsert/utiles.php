<!DOCTYPE html>
<html>
<head>
	<title>Utiles</title>

    <script type="text/javascript">

$(document).ready(function(){

         
        $("#addd5").click(function(){
            // Obtenemos el numero de filas (td) que tiene la primera columna
            // (tr) del id "tabla"
            var tds=$("#tabla5 tr:first td").length;
            
            // Obtenemos el total de columnas (tr) del id "tabla"
            var trs=$("#tabla5 tr").length;
            var nuevaFila="<tr>";
            
            for(var i=1;i<tds;i++){

                // añadimos las columnas
            nuevaFila +=  "<td><input type='Text' size='80' name='descripC' value=''> </td>"+
                          "<td><input type='Number' name='cantC' value=''> </td>";
            }

            // Añadimos una columna con el numero total de filas.
            // Añadimos uno al total, ya que cuando cargamos los valores para la
            // columna, todavia no esta añadida
            //nuevaFila+="<td>"+(trs+1)+" filas";
            //nuevaFila+="</tr>";
            $("#tabla5").append(nuevaFila);
        });

         /**
         * Funcion para eliminar la ultima columna de la tabla.
         * Si unicamente queda una columna, esta no sera eliminada
         */
        $("#ddel5").click(function(){
            // Obtenemos el total de columnas (tr) del id "tabla"
            var trs=$("#tabla5 tr").length;
            if(trs>2)
            {
                // Eliminamos la ultima columna
                $("#tabla5 tr:last").remove();
            
            
            }
        });

    });

    
</script>
</head>
<body>
	<form>

<div class="accordion" id="accordionExample275">
  <div class="card z-depth-0 bordered">
    <div class="card-header" id="headingut">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseut"
          aria-expanded="true" aria-controls="collapseut">
<h2 align="left">C) Descripción de utiles, herramientas y/u otros materiales que quedarán en <br>
        poder del alumnos(deben ir mencionados en las letras A y B)</h2>

        </button>
      </h5>
    </div>
    <div id="collapseut" class="collapse" aria-labelledby="headingut" data-parent="#accordionExample275">
      <div class="card-body">
    <table>

    <!--Las Id seran las que se referencian en las deferentes funciones Del Script-->
                <td><input name="button" id="addd5"  type=button onclick="agregar()" value="Agregar Fila" ></td>
                <td><input name="button" id="ddel5" type=button onclick="borrarUltima()" value="Eliminar Fila" ></td>
        </table>

        <table id="tabla5" border="1 px">
            <tr>
                <td>Descripción</td>
                <td>Cantidad</td>
            </tr>
            <tr>
                <td><input type='Text' size="80" name='descripC' value=''> </td>
                <td><input type='Number' name='cantC' value=''> </td>
            </tr>

        </table><br>


      </div>
    </div>
  </div>
  
</div>
		

         
	</form>

</body>

<script src="<?php echo base_url()?>js/jquery.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
</html>