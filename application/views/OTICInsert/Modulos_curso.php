<!DOCTYPE html>
<html>
<head>
	<title></title>
<script src="http://code.jquery.com/jquery-latest.js"></script>

    <script type="text/javascript">

$(document).ready(function(){

         var NroModulo=2;
        
        $("#add").click(function(){
            // Obtenemos el numero de filas (td) que tiene la primera columna
            // (tr) del id "tabla"
            var tds=$("#tabla tr:first td").length;
            // Obtenemos el total de columnas (tr) del id "tabla"
            var trs=$("#tabla tr").length;
            var nuevaFila="<tr>";
            
            for(var i=2;i<tds;i++){

                // añadimos las columnas
                nuevaFila += "<td><input type='Number' name='NroModulo[]' value='"+NroModulo+"' > </td>"+
                             "<td><input type='Text' name='NombreModulo[]'> </td>"+
                             "<td><input type='Number' step='0.1' class='hrs' name='HorasModulo[]' onkeypress='return check(event)'> </td></tr>";
                NroModulo=NroModulo+1;


             }
            // Añadimos una columna con el numero total de filas.
            // Añadimos uno al total, ya que cuando cargamos los valores para la
            // columna, todavia no esta añadida
            //nuevaFila+="<td>"+(trs+1)+" filas";
            //nuevaFila+="</tr>";
            $("#tabla").append(nuevaFila);
        });

        $("#del").click(function(){
            // Obtenemos el total de columnas (tr) del id "tabla"
            var trs=$("#tabla tr").length;
            if(trs>2)
            {
                // Eliminamos la ultima columna
                $("#tabla tr:last").remove();
                NroModulo=NroModulo-1;
            
            }
        });
     });

   //funcion que permire calcular las Horas de los modulos mediante los imputs de clase hrs
     function calcular_total_h() {
    importe_total = 0
    $(".hrs").each(
        function(index, value) {
            if (eval($(this).val())==null){


            }else{
                importe_total = importe_total + eval($(this).val());
            }
            
        }
    );
    $("#total").val(importe_total);
    }

    //la funcion check permite que solo se ingresen numeros en las casillas donde se requiera 
    function check(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8 ) {
         return true;
        }

        // Patron de entrada, en este caso solo acepta numeros 
        patron = /[0-9,]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }
    
</script>

</head>
<body>

<div class="accordion" id="accordionExample275">
  <div class="card z-depth-0 bordered">
    <div class="card-header" id="headingm">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapsem"
          aria-expanded="true" aria-controls="collapsem">

    <h2>IV.- Módulos del Curso </h2>        

        </button>
      </h5>
    </div>
    <div id="collapsem" class="collapse" aria-labelledby="headingm" data-parent="#accordionExample275">
      <div class="card-body">
  
    <table>
    <!--Las Id seran las que se referencian en las deferentes funciones Del Script -->
            <td><input name="button" id="add"  type=button onclick="" value="Agregar Fila" ></td>
            <td><input name="button" id="del" type=button onclick="" value="Eliminar Fila" ></td>
    </table>
    
        <table id="tabla" border="1 px">
            
            <tr>
                <td><strong>Número Del Módulo</strong></td>
                <td><strong>Nombre del Módulo</strong></td>
                <td><strong>Horas</strong></td>
            </tr>
            <tr>
                <td><input type='Number' name='NroModulo[]' value='1'> </td>
                <td><input type='Text' name='NombreModulo[]'> </td>
                <td><input type='Number'step="0.1" class='hrs' name='HorasModulo[]' onkeypress="return check(event)"> </td>
            </tr>
        </table>
            
        <table>
            <tr>
                <td><label for="total">Total de Horas: <input type="text" name="totalHoras" id="total" value="0"/></td>  
                <td><input type="button" name="btnResul" value="Calcular horas Totales" onclick="calcular_total_h()"></td>
            </tr>
        </table>
       

        <input type="number" name="recon" value='1' hidden="true" >
      </div>
    </div>
  </div>
  
</div>


</body>

<script src="<?php echo base_url()?>js/jquery.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
</html>