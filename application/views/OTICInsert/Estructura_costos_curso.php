<!DOCTYPE html>
<html>
<head>
   
    <script type="text/javascript"></script>
	<title></title>
</head>
<body>
	<form>


<div class="accordion" id="accordionExample275">
  <div class="card z-depth-0 bordered">
    <div class="card-header" id="headingxi">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapsexi"
          aria-expanded="true" aria-controls="collapsexi">

<h2> XI.- Estructura de costos del curso  </h2>
        </button>
      </h5>
    </div>
    <div id="collapsexi" class="collapse" aria-labelledby="headingxi" data-parent="#accordionExample275">
      <div class="card-body">
    
     <table border="1px">
        <tr>
         <td>Item</td>
         <td>Sigla</td>
         <td>Concepto</td>
         <td>Fórmula</td>
         <td>Valor $</td>
       </tr>
         <tr>
         <td>1.- Valor hora Alumno</td>
         <td>VHA</td>
         <td>Corresponde al valor Capacitación <br>
             dividido por el número de alummnos<br>
             y el numero de horas de la face lectiva </td>
         <td>VC/N° horas Fase lectiva/N° cupo</td>
         <td><input  type="Number" name="numxi1"></td>
       </tr>
         <tr>
         <td>2.- Valor alumno subsidio<br>
                de herramientas</td>
         <td>VASUBH</td>
         <td>Este campo lo deben completar sólo <br>
             los cursos de mandato ya que el resto   <br>
             de los cursos debe ser 220.000 según <br> 
             el requerimiento del curso </td>
         <td>No aplica</td>
         <td> <input  type="Number" name="numxi2"></td>
       </tr>
         <tr>
         <td>3.- Valor alumno subsidio<br>
                 para certificaciones <br>
                 y/o licitaciones</td>
         <td>VA<br>
             SUBCLH</td>
         <td>Corresponde al valor por alumno de la <br>
             obtención de la certificación</td>
         <td>No aplica</td>
         <td><input  type="Number" name="numxi3"></td>
       </tr>
         <tr>
         <td>4.- Valor Capacitación </td>
         <td>VC</td>
         <td>Corresponde a los costos en que incurre <br>
            el OTEC para la ejecución de la face <br>
            lectiva del curso, incluido el componente<br>
            técnico Transversal.Incluye los costos<br>
            directos e indirectos de la capacitacion<br>
            y el margen definido por el OTEC</td>
         <td>Sin Fórmula,valor que determina Otec.<br>
            (no incluidos Subsidios)</td>
         <td><input  type="Number" name="numxi4"></td>
       </tr>
         <tr>
         <td>5.- Valor Total práctica <br>
                laboral</td>
         <td>VLP</td>
         <td>Corresponde al valor que se cancela al<br>
             OTEC por la realización de la práctica <br>
             laboral de los alumnos de cursos si es <br>
             que consideran esta fase. El monto por <br>
             alumno del pago por práctica laboral   <br>
             será de $60.000</td>
         <td>Cupo X $60.000</td>
         <td><input  type="Number" name="numxi5"></td>
       </tr>
         <tr>
         <td>6.- Valor Total Asistencia<br>
                Téncica</td>
         <td>VAT</td>
         <td>Corresponde al valor que se cancela al<br>
             al OTEC por la realización de la <br>
             asistencia técnica a los alumnos del<br>
             curso si es que se considera en esta<br>
             fase. El valor por alumno a pagar por<br>
            asistencia técnica realizada será de <br>
            $5.000 </td>
         <td>Cupo X 4 horas X $5.000</td>
         <td><input  type="Number" name="numxi6"></td>
       </tr>
         <tr>
         <td>7.- Valor Total Subsidio <br>
                 Fase Lectiva</td>
         <td>SFL</td>
         <td>Corresponde a $3.000 por alumno por <br>
             dia asistido a la fase lectiva por <br>
             concepto de movilización y/o colación</td>
         <td>Cupo X días fase lectiva <br>
             X $3000</td>
         <td><input  type="Number" name="numxi7"></td>
       </tr>
         <tr>
         <td>8.- Valor Total Subsidio <br>
                fase Práctica Laboral</td>
         <td>SFPL</td>
         <td>Corresponde al monto definido por <br>
             concepto de movilización y/o <br>
             colación por alumno por día asistido<br>
             a la Fase Práctica Laboral </td>
         <td>Cupo X Días Fase experiencia <br>
             laboral X $3.000</td>
         <td><input  type="Number" name="numxi8"></td>
       </tr>
         <tr>
         <td>9.- Valor Total Subsidio de <br>
             útiles y herramientas </td>
         <td>SUBH</td>
         <td>Corresponde al monto definido para <br>
             subsidio de herramientas y es de  <br>
             $220.000 por alumno para cursos de <br>
             modalidad independiente</td>
         <td>Cupo X VASUBH</td>
         <td><input  type="Number" name="numxi9"></td>
       </tr>
         <tr>
         <td>10.- Valor total subsidio para <br>
                certificaciones y/o licencias <br>
                Habilitantes para el Oficio</td>
         <td>SUBCLH</td>
         <td>Corresponde al valor de la obtencion <br>
             de la certificacion o licencia <br>
             habilitante por alumno </td>
         <td>Cupo X Valor certificacion o licencia </td>
         <td><input  type="Number" name="numxi10"></td>
       </tr>
         <tr>
         <td>11.- Valor total del Curso </td>
         <td>VTC</td>
         <td>Corresponde a la suma de los valores <br>
             definidos en los ítems Presedentes</td>
         <td>Suma de Items Anteriores</td>
         <td><input  type="Number" name="numxi11"></td>
   
     </table><br>


      </div>
    </div>
  </div>
  
</div>


		

    
	</form>

</body>

<script src="<?php echo base_url()?>js/jquery.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
</html>