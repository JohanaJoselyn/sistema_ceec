<!DOCTYPE html>
<html>
<head>

    <script type="text/javascript">

    
$(document).ready(function(){

        /**
         * Funcion para añadir una nueva columna en la tabla
         */
         
        $("#add2").click(function(){
            // Obtenemos el numero de filas (td) que tiene la primera columna
            // (tr) del id "tabla"
            var tds=$("#tabla2 tr:first td").length;
            // Obtenemos el total de columnas (tr) del id "tabla"
            var trs=$("#tabla2 tr").length;
            var nuevaFila="<tr>";
            
            for(var i=1;i<tds;i++){

                // añadimos las columnas
            nuevaFila += "<td><input type='text' size='80' name='contenido'> </td>"+
    "<td><input class='HPT' type='numeric' name='ConTeorica' onkeypress='return check(event)'></td>"+  "<td><input class='HPT' type='numeric' name='ConPractica' onkeypress='return check(event)'></td></tr>";
   


             }
            // Añadimos una columna con el numero total de filas.
            // Añadimos uno al total, ya que cuando cargamos los valores para la
            // columna, todavia no esta añadida
            //nuevaFila+="<td>"+(trs+1)+" filas";
            //nuevaFila+="</tr>";
            $("#tabla2").append(nuevaFila);
        });

         /**
         * Funcion para eliminar la ultima columna de la tabla.
         * Si unicamente queda una columna, esta no sera eliminada
         */
        $("#del2").click(function(){
            // Obtenemos el total de columnas (tr) del id "tabla"
            var trs=$("#tabla2 tr").length;
            if(trs>3)
            {
                // Eliminamos la ultima columna
                $("#tabla2 tr:last").remove();
            
            
            }
        });


    });

        



    //funcion que suma todos los campos de las horas teoricas y practicas
 function TotalPracTeo() {
    importe_total = 0
    $(".HPT").each(
        function(index, value) {
            if (eval($(this).val())==null){


            }else{
                importe_total = importe_total + eval($(this).val());
            }
            
        }
    );
    $("#totpT").val(importe_total);
    }



    //la funcion check permite que solo se ingresen numeros en las casillas donde se requiera 
    function check(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8 ) {
         return true;
        }

        // Patron de entrada, en este caso solo acepta numeros 
        patron = /[0-9,]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }
    
</script>
	
</head>
<body>
	<form>


<div class="accordion" id="accordionExample275">
  <div class="card z-depth-0 bordered">
    <div class="card-header" id="headingiv">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseiv"
          aria-expanded="true" aria-controls="collapseiv">
        <h2>VI.- Contenidos </h2>


        </button>
      </h5>
    </div>
    <div id="collapseiv" class="collapse" aria-labelledby="headingiv" data-parent="#accordionExample275">
      <div class="card-body">
     <table>
             <!--Las Id seran las que se referencian en las deferentes funciones Del Script     -->
             <td><input name="button" id="add2"  type=button onclick="" value="Agregar Fila" ></td>
             <td><input name="button" id="del2" type=button onclick="" value="Eliminar Fila" ></td>
        </table>

        <table  id="tabla2" border="1 px">
             
            <tr>
                <td> Contenidos </td>
                <td colspan="2"> Horas Cronológicas </td>
            </tr>
            <tr>
                <td><td>Teóricas</td><td>Prácticas</td></td>
            </tr>
            <tr>
                <td><input type="text" size='80' name='TablaCont'> </td>
                <td><input class="HPT" type="numeric" name='ConTeorica' onkeypress="return check(event)"></td>  
                <td><input class="HPT" type="numeric" name='ConPractica' onkeypress="return check(event)"></td>
             </tr>
        
       
        </table>
        <table>
                <tr>
                    <td><label for="totalprteo">Total de Horas: <input type="text" id="totpT" value="0"/></td>  
                    <td><input type="button" name="btnResul" value="Calcular horas Totales" onclick="TotalPracTeo()"></td>
               
                </tr>
                
        </table>



      </div>
    </div>
  </div>
  
</div>

	</form>
</body>


<script src="<?php echo base_url()?>js/jquery.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
</html>