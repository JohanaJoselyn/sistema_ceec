<!DOCTYPE html>
<html>
<head>
	
    <script type="text/javascript"></script>

	<title>Metodología para equidad de género.</title>

</head>
<body>

<div class="accordion" id="accordionExample275">
  <div class="card z-depth-0 bordered">
    <div class="card-header" id="heading76">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse76"
          aria-expanded="true" aria-controls="collapse76">
         
	<h3 align="left">7.6 Metodología para equidad de género. Especifíque que acciones realizará durante la ejecución de la propuesta que aseguren la igualda de acceso a los beneficios del proyecto y la participación en las diferentes etapas del proceso, tanto a hombres y mujeres, para cada siguiente etapa. Sea breve en cada tema.</h3>
        </button>
      </h5>
    </div>
    <div id="collapse76" class="collapse" aria-labelledby="heading76" data-parent="#accordionExample275">
      <div class="card-body">
       <table width="">
				<TR>
					
					<TH>Ejecución del proyecto</TH>
					<TD><input type="text" name="ejecProy" id="ejecProy" required=""></TD>
				</TR>
				<TR>
					<TH>Participación  de hombres</TH> 
					<TD><input type="text" name="partiHombre" id="partiHombre" required=""></TD>
					
				</TR>
				<TR>
					<TH>Participación de mujeres</TH> 
					<TD><input type="text" name="partiMujer" id="partiMujer" required=""></TD>
				</TR>
			</table><br>

      </div>
    </div>
  </div>
  
</div>


			

</body>

<script src="<?php echo base_url()?>js/jquery.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
</html>