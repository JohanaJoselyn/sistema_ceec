<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class criterios extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('criteriosModel');
		$this->load->model('modulos_MODEL');
		$this->load->model('metodologiaOtic_MODEL');
		$this->load->model('propuesta_MODEL');
	}

	
	function cargarotic(){
		$this->load->view('OTIC/Form_OTIC');
	}
	function cargarfosis(){
		$this->load->view('FOSIS/formularioFosis');
	}

//ESTA FUNCIÓN OCUPO PARA LA VISTA Y OCUPO FUNCIÓN DEL MODELO 10-4//
	function combobox(){
		$data = $this->criteriosModel->verentidad();
		$this->load->view('Form_dinamicos/Form_crit',['entidad'=>$data]);
		
	}

	//COMBOBOX ANIDADO, FUNCIONA ¬¬!
	function anidado(){
		$nombre_programa=$this->input->post('nombre_programa');
		$id_entidad=$this->input->post('id_entidad');

		if ($nombre_programa=="programas") {
			$this->load->model('criteriosModel');
			echo $this->criteriosModel->programas($id_entidad);
		}

	}


	function check(){

		$myarray= array();

		//OTIC

		if (isset($_POST['Informacion']))  {
			$seleccionar3 = $this->input->post('Informacion');
			print_r($seleccionar3);
			$vista=$this->load->view('OTICInsert/OTEC',null,true);
			array_push($myarray, $vista);
		}

		if (isset($_POST['entidad']))  {
			$seleccionar4 = $this->input->post('entidad');
			print_r($seleccionar4);
			$vista2=$this->load->view('OTICInsert/Entidad',null,true);
			array_push($myarray, $vista2);
		}

		if (isset($_POST['Curso']))  {
			$seleccionar4 = $this->input->post('Curso');
			print_r($seleccionar4);
			$vista3=$this->load->view('OTICInsert/curso',null,true);
			array_push($myarray, $vista3);
		}

		if (isset($_POST['Modulos'])) {
			$seleccionar = $this->input->post('Modulos');
			print_r($seleccionar);
			$vista4=$this->load->view('OTICInsert/Modulos_curso',null,true);
			array_push($myarray, $vista4);
	

		}

		if (isset($_POST['Aprendizaje'])) {
			$seleccionar = $this->input->post('Aprendizaje');
			print_r($seleccionar);
			$vista5=$this->load->view('OTICInsert/Aprendizaje',null,true);	
			array_push($myarray, $vista5);

		}

		if (isset($_POST['Contenido'])) {
			$seleccionar2 = $this->input->post('Contenido');
			print_r($seleccionar2);
			$vista6=$this->load->view('OTICInsert/Contenidos',null,true);
			array_push($myarray, $vista6);
		}

		if (isset($_POST['Metodologia'])) {
			$seleccionar2 = $this->input->post('Metodologia');
			print_r($seleccionar2);
			$vista7=$this->load->view('OTICInsert/Metodologia',null,true);
			array_push($myarray, $vista7);
		}

		if (isset($_POST['Criteriosq'])) {
			$seleccionar2 = $this->input->post('Criterios');
			print_r($seleccionar2);
			$vista8=$this->load->view('OTICInsert/Criterio_evaluacion',null,true);
			array_push($myarray, $vista8);
		}

		if (isset($_POST['Materiales'])) {
			$seleccionar2 = $this->input->post('Materiales');
			print_r($seleccionar2);
			$vista9=$this->load->view('OTICInsert/Recurso_curso',null,true);
			array_push($myarray, $vista9);
		}

		if (isset($_POST['Equipos'])) {
			$seleccionar2 = $this->input->post('Equipos');
			print_r($seleccionar2);
			$vista10=$this->load->view('OTICInsert/Equipos',null,true);
			array_push($myarray, $vista10);
		}

		if (isset($_POST['Utiles'])) {
			$seleccionar2 = $this->input->post('Utiles');
			print_r($seleccionar2);
			$vista11=$this->load->view('OTICInsert/utiles',null,true);
			array_push($myarray, $vista11);
		}

		if (isset($_POST['Infraestructura'])) {
			$seleccionar2 = $this->input->post('Infraestructura');
			print_r($seleccionar2);
			$vista12=$this->load->view('OTICInsert/Infraestructura',null,true);
			array_push($myarray, $vista12);
		}

		if (isset($_POST['Duración'])) {
			$seleccionar2 = $this->input->post('Duración');
			print_r($seleccionar2);
			$vista13=$this->load->view('OTICInsert/Duracion_curso',null,true);
			array_push($myarray, $vista13);
		}

		if (isset($_POST['Estructura'])) {
			$seleccionar2 = $this->input->post('Estructura');
			print_r($seleccionar2);
			$vista14=$this->load->view('OTICInsert/Estructura_costos_curso',null,true);
			array_push($myarray, $vista14);
		}

		if (isset($_POST['Otros'])) {
			$seleccionar2 = $this->input->post('Otros');
			print_r($seleccionar2);
			$vista15=$this->load->view('OTICInsert/Otros_datos',null,true);
			array_push($myarray, $vista15);
		}

	//FOSIS

		if (isset($_POST['identificacion'])) {
			$seleccionar2 = $this->input->post('identificacion');
			print_r($seleccionar2);
			$vista16=$this->load->view('FOSIS/Informacion_general',null,true);
			array_push($myarray, $vista16);
		}

		if (isset($_POST['propuesta'])) {
			$seleccionar2 = $this->input->post('propuesta');
			print_r($seleccionar2);
			$vista17=$this->load->view('FOSIS/Propuesta',null,true);
			array_push($myarray, $vista17);
		}

		if (isset($_POST['Infraestructuraf'])) {
			$seleccionar2 = $this->input->post('Infraestructuraf');
			print_r($seleccionar2);
			$vista18=$this->load->view('FOSIS/Infraestructura_y_equipamiento',null,true);
			array_push($myarray, $vista18);
		}

		if (isset($_POST['Formacion'])) {
			$seleccionar2 = $this->input->post('Formacion');
			print_r($seleccionar2);
			$vista19=$this->load->view('FOSIS/Recursos_humanos',null,true);
			array_push($myarray, $vista19);
		}

		if (isset($_POST['Experiencias'])) {
			$seleccionar2 = $this->input->post('Experiencias');
			print_r($seleccionar2);
			$vista20=$this->load->view('FOSIS/experiencia_institucional',null,true);
			array_push($myarray, $vista20);
		}

		if (isset($_POST['Presupuesto'])) {
			$seleccionar2 = $this->input->post('Presupuesto');
			print_r($seleccionar2);
			$vista21=$this->load->view('FOSIS/Presupuesto',null,true);
			array_push($myarray, $vista21);
		}

		if (isset($_POST['metodologia_fosis'])) {
			$seleccionar2 = $this->input->post('metodologia_fosis');
			print_r($seleccionar2);
			$vista22=$this->load->view('FOSIS/Metodologia',null,true);
			array_push($myarray, $vista22);

		}

		if (isset($_POST['Innovacion'])) {
			$seleccionar2 = $this->input->post('Innovacion');
			print_r($seleccionar2);
			$vista23=$this->load->view('FOSIS/InnovacionMetodologia',null,true);
			array_push($myarray, $vista23);
		}

		if (isset($_POST['Enfoque'])) {
			$seleccionar2 = $this->input->post('Enfoque');
			print_r($seleccionar2);
			$vista24=$this->load->view('FOSIS/enfoque_generoM',null,true);
			array_push($myarray, $vista24);
		}

		if (isset($_POST['Difusion'])) {
			$seleccionar2 = $this->input->post('Difusion');
			print_r($seleccionar2);
			$vista25=$this->load->view('FOSIS/Difusion',null,true);
			array_push($myarray, $vista25);
		}

		$this->load->view('envio',array('recibodatos'=>$myarray));

	}

	//LA FUNCION EJECUTAR COMPROVARA SI REALMENTE EXISTEN LOS DIFERENTES FORMULARIOS PARA LLAMAR A LOS METODOS DE INSERCION
		function ejecutar(){
		$this->ultimos();	
		$mod=1;
		if($mod==isset($_POST['recon'])){
			 
		$this->insertarModulos();
		}
 		$this->insertarMetodologia();
 		
    	}

		//la funcion ultimos recupera el ultimo id de la tabla propuesta que nos ayudara a saber a que propuesta pertenece cada formulario
		function ultimos(){
			$ulti= $this->propuesta_MODEL->ultimo();
			//el for each recorre el array recuperado de la consulta
			foreach ($ulti -> result() as $ultis) {
				//esta linea muestra por pantalla la id actual antes de la insercion
				echo $ultis->id_propuesta.'';
				$np=intval($ultis->id_propuesta)+1;
				//esta linea inserta a la base de datos
			   echo $np;

			}
			$this->propuesta_MODEL->nuevaprop($np);
		
		}




	//funcion que llama al modelo metodologia
	function insertarMetodologia(){
		$np = 1;
		$ulti= $this->propuesta_MODEL->ultimo();
			foreach ($ulti -> result() as $ultis) {
				//echo $ultis->id_propuesta.'';
			  $np=intval($ultis->id_propuesta);
			   
		}
			   

		if(isset($_POST['metoOt'])){
			$desc=$_POST['metoOt'];
			$this->metodologiaOtic_MODEL->insertarMet($desc,$np);
		}

	
}
	
	//funcion que llama a la insrercion de modulo
	function insertarModulos(){	

	$np = 0;
		$ulti= $this->propuesta_MODEL->ultimo();
			foreach ($ulti -> result() as $ultis) {
			//echo $ultis->id_propuesta.'';
			  $np=intval($ultis->id_propuesta);
			   
		}
      //SE RECIBEN LOS ARRAY QUE SE ATRAPAN EN EL FORMULARIO DE MODULOS //
 	$items1 =($_POST['NroModulo']);
 	$items2 =($_POST['NombreModulo']);
 	$items3 =($_POST['HorasModulo']);

 			//SE CREAR UN CICLO WHILE PARA RECORRE CADA UNO DE LOS ARRAY ANTES GUARDADOS CON EL FIN DE SEPARAR LOS VALORES
 			while(true){

 				//SE ASIGNA A UNA NUEVA VARIABLE DONDE POR CADA ITERACION DEL CICLO SE GUARDARA EL NUEVO VALOR
 				$item1=current($items1);
 				$item2=current($items2);
 				$item3=current($items3);

 				//CADA ITEM SE ASIGANA A UNA VARIABLE 
 				$nromo =(($item1!==false)? $item1 :", &nbsp;");
				$nomo  =(($item2!==false)? $item2 :", &nbsp;");
				$hrsmo =(($item3!==false)? $item3 :", &nbsp;");

				// LAS VARIABLES ANTES ASIGNADAS SE CONCATENAN CREANDO LOS VALORES QUE SE ENVIARAN LISTOS PARA SU INSERCION
				$valores='('.$nromo.',"'.$nomo.'","'.$hrsmo.'","'.$np.'"),';

				//YA QUE LA VARIABLE ANTERIOS TERMINA EN UNA COMA, CON LA FUNCION SUBSTR LA ELIMINAREMOS EL ULTIMO CARRACTER DE LA FILA
				$valoresQ=substr($valores, 0,-1);

				// DENTO DEL WHILE SE ENVIAN LOS PARAMETROS SEPARADOS EN UNA CONSULTA QUE IRA DIRECTAMENTE A VALUES DE LA INSERCION QUE ESTA EN EL MODELO.
 				$this->modulos_MODEL->insertar($valoresQ);

 				//EN ESTE PUNTO SE LE ASIGNARA EL SIGUENTE VALOR DEL ARRAY A LA VARIABLE INDEPENDIENTE 
 				$item1=next($items1);
 				$item2=next($items2);
 				$item3=next($items3);

 				//ESTA CONSULTA EVALUA SI ACASO HAY MAS DATOS DENTRO DE LOS ARRAY, DEBEN CUMPLIRSE TODAS LAS CONDICOINES PARA QUE EL CICLO WHILE TERMINE
 				if($item1===false && $item2===false && $item3===false) break ;

 			}
	}





}


?>