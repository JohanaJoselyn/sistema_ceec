<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class visualizacion extends CI_Controller {

	function __construct(){

		parent::__construct();
		$this->load->model('visualizacionModel');
		
		
		$this->load->view('Estaticas/header');

}

	function visualizar(){

			// SE RECUPERA EL ID DE CURSO AL PRESIONAR EL CURSO ADECUADO EN LA TABLA
			$id = $this->uri->segment(3);

			//VARIABLES QUE ALMACENAN EL REDIRECCIONAMIENTO DEL MODELO.
			$visualizarentidad = $this->visualizacionModel->combobox();
		  	$obtener = $this->visualizacionModel->obtenervalores($id);
		    $select = $this->visualizacionModel->selecciones($id);
		    $entidad = $this->visualizacionModel->verentidad();


		    /* SE RECUPERA CADA ATRIBUTO DE LA BASE DE DATOS Y SE ALMACENA EN UNA VARIABLE, LUEGO ESTAS VARIABLES SE RECORREN POR CADA POST
		    RECUPERADO DE LA VISTA (VALUE) */

			//if ($obtenernombre !=FALSE | $visualizarentidad !=FALSE) {
				//foreach ($obtenernombre->result() as $row) {
					//foreach ($visualizarentidad->result() as $r) {
		    if ($select !=FALSE | $obtener !=FALSE | $entidad !=FALSE) {
		    	foreach ($select->result() as $r) {
		    		foreach ($obtener->result() as $row) {
		    			
		    				
		    			
		    		
					$nombre_curso = $row->nombre_curso;
					$codigo_curso = $row->id_curso;
					$comuna_curso = $row->comuna_curso;
					$region_curso = $row->region_curso;
					$tipo_curso = $row->tipo_curso;
					$salida_curso = $row->salida_curso;
					$cupos_curso = $row->cantidad_alumnos;
					$componentes_curso = $row->componentes_curso;
					$programa = $row->nombre_programa;
					$entidad = $r->nombre;
					$rutt = $r->rut_entidad;

					//equipos

					$descripcion = $r->descripcion;
					$CantidadM = $r->cantidad;
					$Antiguedad = $r->antiguedad;
					$CertificacionAso = $r->certificacion_normas;


					//metodologia

					//$metodologia = $row->descripcion_m;

					//modulos curso

					$NroModulo = $r->numero_modulo;
					$NombreModulo = $r->nombre_modulo;
					$HorasModulo = $r->horas;

					//aprendizaje
					$aprendizaje = $r->descripcion;
						
					}
				}
			
		//	}

				//SE COMPARA EL POST CON LA VARIABLE ANTERIOR Y SE ALMACENA EN UN ARRAY
				$data = array(
					'id_curso' => $id,
					'nombre_curso' => $nombre_curso,
					'codigo_curso' => $codigo_curso,
					'comuna_curso' => $comuna_curso,
					'region_curso' => $region_curso,
					'tipo_curso'   => $tipo_curso,
					'salida_curso' => $salida_curso,
					'cupos_curso'  => $cupos_curso,
					'componentes_curso' => $componentes_curso,
					'nombre_programa'=> $programa,
					'nombre' => $entidad,
					'rut' => $rutt
				);
				$data2  = array(
					'descripcion'=> $descripcion,
					'CantidadM' =>  $CantidadM,
					'Antiguedad' => $Antiguedad,
					'CertificacionAso' => $CertificacionAso,

					//modulos cursos
					'NroModulo' => $NroModulo,
					'NombreModulo' => $NombreModulo,
					'HorasModulo' => $HorasModulo,

					//'descripcionm' => $metodologia
					
					//aprendizaje
					'aprendizaje_otic' => $aprendizaje

				);
			}else{
				$data ='';
				return FALSE;
			}
			//$this->load->view('OTIC/curso',$data);

			// VISTAS CON METODO Y ARRAY CORRESPONDIENTE
			$this->load->view('OTICSelect/OTEC',array('entidad' => $visualizarentidad,'data'=>$data));
			$this->load->view('OTICSelect/curso',array('entidad' => $visualizarentidad,'data'=>$data));
			$this->load->view('OTICSelect/Aprendizaje',array('data'=>$data2));
			$this->load->view('OTICSelect/Entidad',array('data'=>$data));
			$this->load->view('OTICSelect/Equipos',array('data'=>$data2));
			$this->load->view('OTICSelect/Modulos_curso',array('data'=>$data2));

			
			//$this->load->view('OTIC/metodologia',array('entidad' => $visualizarentidad,'metodologia' =>$select));


		

	}


	function editar(){
		$id = $this->uri->segment(3);
		$data = array(
			'curso' =>  $this->input->post('nombre_curso',true)
			 );
		$this->visualizacionModel->editar($id,$data);
		//me falta pasarle la vista :D

	}

	function insertar(){
		$nombre_curso = $this->input->post('nombre_curso');
		$codigo_curso = $this->input->post('codigo_curso');
		$comuna_curso = $this->input->post('comuna_curso');
		$region_curso = $this->input->post('region_curso');
		$tipo_curso   = $this->input->post('tipo_curso');
		$salida_curso = $this->input->post('salida_curso');
		$cupos_curso  = $this->input->post('cupos_curso');
		$componentes_curso = $this->input->post('componentes_curso');
		$id_programa = $this->input->post('nombre_programa');

		$data = array('nombre_curso' => $nombre_curso,
					  'codigo_curso' => $codigo_curso,
					  'comuna_curso' => $comuna_curso,
					  'region_curso' => $region_curso,
					  'tipo_curso'   => $tipo_curso,
					  'salida_curso' => $salida_curso,
					  'cantidad_alumnos'  => $cupos_curso,
					  'componentes_curso'=> $componentes_curso,
					  'nombre_programa'=> $id_programa

					);
		$this->load->model('visualizacionModel');

		if($this->visualizacionModel->insertar($data)){
			echo "inserto la cosa";
		}else{
			echo "no lo hizo :C";
		}
	}




}




?>






