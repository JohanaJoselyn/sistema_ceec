<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class buscador extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('buscadorModel');
		$this->load->model('archivoModel');
		$this->load->view('Estaticas/header');

		
	}

	
//MOSTRAR REGISTROS EN LA TABLA 
function detalle (){

	if ($_POST) {
		$buscar = $this->input->post('busqueda');
	}
	else{
		$buscar='';
	}
/*	$data = array(
		'curso' => $this->buscadorModel->vertodo($buscar)
	);*/


	
	$data['curso']= $this->buscadorModel->vertodo($buscar);
	//$data['programas']= $this->buscadorModel->programa($buscar);
	$this->load->view('Filtros/busqueda',$data);
	$this->load->view('Estaticas/footer');
}

//OBTENER ESTADO
function editar(){
	$id = $this->uri->segment(3);
	$obtenerEstado = $this->buscadorModel->obtenerEstado($id);

	if($obtenerEstado != FALSE){
		foreach ($obtenerEstado->result() as $row) {
			//$estado = $row->estado;
			$estado = $row->estado;
		}
		$data = array(
			'id' => $id,
			'estado' => $estado
		);
	}else{
		$data='';
		return FALSE;
	}

	$this->load->view('Filtros/editar',$data);
			
		}

//FUNCION PARA EDITAR EL ESTADO :ADJUDICADO-PERDIDO .-

function editarEstado(){
	$id = $this->uri->segment(3);
	$data = array(
		'estado' =>$this->input->post('estado',true),
	);
	$this->buscadorModel->editarEstado($id,$data);
	redirect('buscador/detalle');
}

//PDF
function verArchivo($idA)
  {
    $consulta = $this->archivoModel->getArchivo($idA);
    $archivo = $consulta['curso'];
    $nombre= $consulta['archivos_link'];
    //$data['curso']= $this->buscadorModel->getArchivo($idA);
    header("Content-type: application/pdf");
    header("Content-Disposition: inline; filename=$nombre.pdf");
    print_r($consulta);
    

  
  }
	}

	


  ?>